# ZurichDynamicForm

This project was generated using [Nx](https://nx.dev).

## Quick Start & Documentation

### Dependencies Globals
```
    >  [Nx Framework](https://nx.dev/latest/angular/cli/overview)
    >  [JavaScriptObfuscator](https://github.com/javascript-obfuscator/javascript-obfuscator)
```

## Commands Utils

> Run aplication `ng serve zurich-dynamic-form`.
> Run unit test `npm run affected:test`.
> Run unit e2e `npm run affected:e2e`.
> Run build application `npm run affected:build`.
> Run obfuscate code `npm run post:build`.

## Generate a library

Run `ng g @nrwl/angular:lib my-lib` to generate a library with angular.

> You can also use any of the plugins above to generate libraries as well.

Libraries are sharable across libraries and applications. They can be imported from `@zurich-dynamic-form/mylib`.

## Development server

Run `ng serve zurich-dynamic-form` for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.
Run `ng serve zurich-dynamic-form` project serve

## Code scaffolding

# Generate lib
Run `ng g @nrwl/workspace:lib <name>` to generate a library in libs.

# Add a component
Run `ng g @nrwl/angular:component xyz --project zurich-dynamic-form` to generate a new component in project.
Run `ng g @nrwl/angular:component xyz --project ui` to generate a new component in lib ui.
Run `ng g component my-component --project=zurich-dynamic-form` to generate a new component.

## Build

Run `ng build zurich-dynamic-form` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test zurich-dynamic-form` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.

Run `npm run affected:test` to execute the unit tests affected by a change with coverage.

## Running end-to-end tests

Run `ng e2e zurich-dynamic-form` to execute the end-to-end tests via [Cypress](https://www.cypress.io).

Run `nx affected:e2e` to execute the end-to-end tests affected by a change.

## Understand your workspace

Run `nx dep-graph` to see a diagram of the dependencies of your projects.

## Further help

Visit the [Nx Documentation](https://nx.dev/angular) to learn more.

