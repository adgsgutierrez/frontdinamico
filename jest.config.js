module.exports = {
  projects: [
    '<rootDir>/apps/zurich-dynamic-form',
    '<rootDir>/libs/ui',
    '<rootDir>/libs/services',
    '<rootDir>/libs/schemas',
    '<rootDir>/libs/data',
  ],
};
