export interface IProvince {
    id: string | number;
    value: string;
}

export interface ICities {
    id: string | number;
    value: string;
}

export interface IWhereHappen {
    province: IProvince;
    cities: ICities
}

export interface ICoordinateDamage {
    left : string[];
    right : string[];
    front : string[];
    back : string[];
}

export interface ISinisterData{
    whenHappen : string;
    timeHappened : string;
    whereHappen : IWhereHappen;
    claimAddress: string;
    howHappen: string;
    whatDamages: string;
    coordinateDamage: ICoordinateDamage;
    therePolicePresence: boolean;
    //someKindOfComplaintWasMade: boolean;
    thereInjuriesOccupants: boolean;
    wasAnyComplaintMade: boolean;
    coolantOilOrHydraulicFluidLeaked: boolean;
    canTheVehicleMobilizedByItsOwnMeans: boolean;
    //howManyPiecesWereAffectedInTheCrash: string;
    wasProformaRepairVehicleDamagesCarriedOut: boolean;
    useTheContractedPolicyForRepair: boolean;
    thirdPartyDamage:boolean;
}

export interface IWhoCompletedThisFormGroup{
    whoCompletedThisForm: string ;
    name: string ;
    lastName: string ;
    cellPhone: string ;
}

export interface IDetailsDriverInsuredVehicle {
    theDriverIsTheInsured: boolean;
    licenseNumber: string;
    licenseType: string;
    dueDate: Date;
    namesDriver: string;
    lastNameDriver: string;
    landline : string | number;
    cellPhone : string | number;
    email: string;
    addressOfYourHome: string;
    authorizedDriveBy: string;
    relationshipWithTheInsured: string;
    cityOfSettlementOfTheClaim: string;
    filingDate: string;
    whoCompletedThisFormGroup: IWhoCompletedThisFormGroup;
}

export interface ILossType {
    id: number;
    value: string;
}
export interface ICauseSinisterType {
    id: number;
    value: string;
}
export interface ISinisterCity {
    id: number;
    value: string;
}

export interface IVehicleData {
    idVehicle: string;
    idPolicy: string;
    generatedSinisterToday: boolean;
    policyNumber: number;
    agent: string;
    validSince: Date;
    validUntil: Date;
    productId : string;
    product : string;
    brandId : string;
    brand : string;
    idModel : string;
    model : string;
    vehicleColor : string;
    year : number;
    plates : string;
    chassis : string;
    motor : string;
}

export interface IPolicyAffectedRamo {
    id: number | string;
    value: string;
}

export interface IDocument {
    id: number | string;
    value: string;
    number: number | string;
}

export interface IInformationClientNaturalPerson {
    id: string;
    document: IDocument;
    name : string;
    lastName : string;
    landline : string;
    cellPhone : string;
    email : string;
}

export interface ICreateSinister {
    uuidRequest: string;
    theSinisterIsManual: boolean;
    doAnIntrospection: boolean;
    receiveACallFromTheAdvisor: boolean;
    vehicleIsAffected: boolean;
    inspectionDate: string;
    inspectionHour: string;
}

export interface IListThirdPartyInformation {
    idTypeOfDamage : string;
    typeOfDamage : string;
    idMaterialDamage : string;
    materialDamage : string;
    document: IDocument;
    name : string;
    lastName : string;
    lossAdress : string;
    plates : string;
    chassis : string;
    motor : string;
    email : string;
    cellPhone: string | number;
    descriptionWellAffected : string;
    damageDescription : string;
    coordinateDamage: ICoordinateDamage;
    theAffectedPropertyIsInsured : boolean;
    nameOfTheInsurer : string;
}

export interface IListInjuredPerson {
    listIsTheInjuredPersonTheSameInsuredPerson : boolean;
    name : string;
    lastName : string;
    landline: string | number;
    cellPhone: string | number;
    email : string;
    relationshipWithTheInsured : string;
    sayWhereYouWereAtTheTimeOfTheAccident : string;
}

export interface IListWitnessesToTheAccident {
    name : string;
    lastName : string;
    landline : string | number;
    cellPhone : string | number;
    email : string;
}

export interface IContactPerson{
    name: string;
    lastName: string;
    landline: string | number;
    cellPhone: string | number;
    email: string;
}

export interface IInformationClientLegalPerson {
    id: string;
    numberRUC: IDocument;
    nameLegalPerson: string;
    contactPerson: IContactPerson;
}

export interface ISendData{
    createSinister: ICreateSinister;
    informationClientNaturalPerson ?: IInformationClientNaturalPerson;
    informationClientLegalPerson ?: IInformationClientLegalPerson;
    policyAffectedRamo: IPolicyAffectedRamo;
    vehicleData : IVehicleData;
    lossType: ILossType;
    causeSinisterType: ICauseSinisterType;
    sinisterCity: ISinisterCity;
    sinisterData : ISinisterData;
    detailsDriverInsuredVehicle : IDetailsDriverInsuredVehicle;
    listThirdPartyInformation : IListThirdPartyInformation[];
    listInjuredPerson: IListInjuredPerson[];
    listWitnessesToTheAccident: IListWitnessesToTheAccident[];
    listImagesSinister : string[];
}