export const environment = {
  production: true,
  host : 'https://balanceador.zurichseguros.com.ec',
  // host : '',
  path : '/zurich/api/fnol',
  token : '/token',
  msm : {
    information : 'Estimado cliente, a través de este formulario podrá realizar su reclamación, por lo que le pedimos que lo diligencie oportunamente y además anexe los documentos que se requerirán como soportes. Asegúrese de contar con una conexión estable a internet.',
    policy : 'Entendemos que su reclamación no contemplará los daños del vehículo asegurado, pero si contemplará información correspondiente a terceros afectados. Al continuar al siguiente paso, solicitaremos información acerca de los involucrados'
  },
  crypto: {
    iv: 'DeRT2f2k9/5/5A80',
    secret : 'd6b9c*S?F)H-LnOwDjCnTw4y7q8x#V$V'
  }
};


export const service = {
  token : 'OFplekdWbm9FTllOZzN5Y2VfRGpwc3VKRUdnYTppS2JRTldwUGQ3RWNKQnpUcXRSME5Nb0NMblFh',
    'customerInformation' : {
      method : 'get',
      path : '/customers/1.0.0/customer/@inputTypeDocumentIdentity/@inputNumberDocumentIdentity'
    },
    'informationByPlate' : {
      method : 'get',
      path : '/vehiclebyplate/1.0.0/vehicle/informationByPlate/@inputNumberCarIdentityRegistry/@inputTypeDocumentIdentity/@inputNumberDocumentIdentity'
    },
    'informationByMotor' : {
      method : 'get',
      path : '/vehiclebymotor/1.0.0/vehicle/informationByMotor/@inputNumberMotorCarRegistry/@inputTypeDocumentIdentity/@inputNumberDocumentIdentity'
    },
    'losstype' : {
      method : 'get',
      path : '/parametric/1.0.0/losstype/@ramoForFilter'
    },
    'causesinister' : {
      method : 'get',
      path : '/parametric/sinister/1.0.0/causesinister/@ramoForFilter'
    },
    'listprovince': {
      method : 'get',
      path : '/parametric/province/1.0.0/listprovince'
    },
    'listcities' : {
      method : 'get',
      path :'/parametric/cities/1.0.0/listcities/@inputProvinceSinister'
    },
    'listoffice' : {
      method : 'get',
      path :'/parametric/office/1.0.0/listoffice'
    },
    'uploadftp' : {
      method : 'post',
      path :'/uploadftp/1.0.0'
    },
    'deleteftp' : {
      method : 'post',
      path : '/deleteftp/1.0.0'
    },
    'brand' : {
      method : 'get',
      path :'/vehiclesbrand/1.0.0/brand'
    },
    'brandModel' : {
      method : 'get',
      path :'/vehiclesmodel/1.0.0/model/@inputBrandCarRegistry'
    },
    'createSinister' : {
      method : 'post',
      path : '/sinister/create/1.0.0'
    }
}