import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'zurich-dynamic-form-page-result',
  templateUrl: './page-result.component.html',
  styleUrls: ['./page-result.component.scss']
})
export class PageResultComponent implements OnInit {

  public urlImage = 'assets/img/header-complete-desktop.png';
  public title : SafeHtml = '';

  constructor(private route: ActivatedRoute , private sanitizer:DomSanitizer) { }

  ngOnInit(): void {
    const value = sessionStorage.getItem('complete') || '';
    if (value == '') {
      window.location.href = '/';
    }
    this.route.queryParams.subscribe(
      (querys: {status: string , number: string}) => {
        if (querys.status === 'ok') {
          this.title = this.sanitizer.bypassSecurityTrustHtml(`El número asignado es <span style=" font-weight: bold; text-align: center; padding-bottom: 3px;color: #253760; font-size: 16px; font-family: "ZurichSansBold";">\"${querys.number}\"</span>. En las siguientes 8 horas laborales un ejecutivo le contactará para explicarle el proceso a seguir.`);
        } else {
          this.title = this.sanitizer.bypassSecurityTrustHtml(`En las siguientes 8 horas laborales un ejecutivo le contactará para explicarle el proceso a seguir.`);
        }
      }
    )
  }

  loadUrl(): void {
    sessionStorage.clear();
    window.location.href = 'https://www.zurichseguros.com.ec';
  }

}
