import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentLogicComponent } from './component-logic.component';

describe('ComponentLogicComponent', () => {
  let component: ComponentLogicComponent;
  let fixture: ComponentFixture<ComponentLogicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentLogicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentLogicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
