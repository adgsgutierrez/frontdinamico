import { environment, service } from "@env/environment";
import { IComponent, IOptions } from "@zurich-dynamic-form/schemas";
import { ICauseSinisterType, ICities, ICoordinateDamage, ICreateSinister, 
    IDetailsDriverInsuredVehicle, IDocument, IInformationClientLegalPerson, IInformationClientNaturalPerson, 
    IListInjuredPerson, IListThirdPartyInformation, IListWitnessesToTheAccident, 
    ILossType, IPolicyAffectedRamo, IProvince, ISendData, ISinisterCity, 
    ISinisterData, IVehicleData, IWhereHappen, IWhoCompletedThisFormGroup } from "apps/zurich-dynamic-form/src/interface/request.send";
import { PipeMask } from "libs/ui/src/lib/input-mask/Pipes/PipeMask.transform";
import { ModalComponent } from "libs/ui/src/lib/modal/modal.component";
import { Security } from "libs/ui/src/utils/security";
import { AppComponent } from "../../../app.component";
import { APPLICATION } from "../../../utils/constants";
import { Functions } from "../../../utils/functions";
import { FunctionsComponent } from "../component-logic.functions";

export class LogicPage3{

    private functions: FunctionsComponent;
    private imageThird: string[] = [];

    constructor(fun: FunctionsComponent) { 
        this.functions = fun;
    }

    public showLoadDriver(component: IComponent , data: any) : void {
       this.showSectionDriverInformation(component);
       this.showSectionThirdInformation();      
       this.showSectionInjuredInformation();
       this.showInspectionMethod();
       this.showDataRegisterSiniester();
       this.loadDocumentationInjured();
    }

    public loadDocumentationInjured() : void {
        const bool = this.functions.inspectionExpress();
        if (!bool) {
            this.functions.getComponentForId('@LabelDocumentationInsured').isVisible();
            this.functions.getComponentForId('@inputFrontDocumentRegister').isVisible();
            this.functions.getComponentForId('@inputBackDocumentRegister').isVisible();
            this.functions.getComponentForId('@inputNumberCarVehicleFront').isVisible();
            this.functions.getComponentForId('@inputNumberCarVehicleBack').isVisible();
        } else {
            this.functions.getComponentForId('@LabelDocumentationInsured').isInvisible();
            this.functions.getComponentForId('@inputFrontDocumentRegister').isInvisible();
            this.functions.getComponentForId('@inputBackDocumentRegister').isInvisible();
            this.functions.getComponentForId('@inputNumberCarVehicleFront').isInvisible();
            this.functions.getComponentForId('@inputNumberCarVehicleBack').isInvisible();
        }
    }

    public showDataRegisterSiniester(): void {
        this.functions.getComponentForId('@idRadicacionSiniestro').isVisible();
        const cacheData = this.functions.componentLogic.data.getAllObject();
        const dataCodeCitySiniester = cacheData['@inputCityProcessSinister'];
        const arrayCityOptions = JSON.parse( cacheData['@inputCityProcessSinister_options'] || '[]' );
        let dataLabelCitySiniester = '';
        arrayCityOptions.forEach((data) => {
            if (data.key === dataCodeCitySiniester) {
                dataLabelCitySiniester = data.value;
            }
        });
        const dateRegisterSiniester = new Date();
        let options: IOptions[] = [];
        options.push({
            title: "Ciudad de radicación del siniestro",
            value : dataLabelCitySiniester
        });
        const valueDate = dateRegisterSiniester.getDate() + '/'+ 
            APPLICATION.MONTHS[ dateRegisterSiniester.getMonth() ] + '/' + dateRegisterSiniester.getFullYear();
        options.push({
            title: "Fecha de radicado",
            value : valueDate
        });
        this.functions.getComponentForId('@idRadicacionSiniestro').setOptions(options);
    }

    public showPeopleWitness(component: IComponent , data: any): void {
        const value = this.functions.getComponentForId(component.id).getValue();
        if (value == 'Y') {
            this.functions.getComponentForId('@idCardWitnesses').isVisible();
        } else {
            this.functions.getComponentForId('@idCardWitnesses').isInvisible();
        }
    }

    private showSectionDriverInformation(component: IComponent): void {
        const value = this.functions.getComponentForId(component.id).getValue();
        this.functions.getComponentForId('@inputNameInsured').isVisible();
        this.functions.getComponentForId('@inputLastNameInsured').isVisible();
        this.functions.getComponentForId('@inputCellPhoneInsured').isVisible();
        this.functions.getComponentForId('@inputEmailInsured').isVisible();
        
        if (value == 'Y') {
            const data = this.functions.componentLogic.data.getAllObject();
            const isPostLoadPhone = localStorage.getItem('@inputCellPhoneRegister_postLoad') || '';
            const isPostLoadEmail = localStorage.getItem('@inputEmailRegister_postLoad') || '';
            const typePerson = data['@inputTypeDocumentIdentity'] || '';
            const cell = (isPostLoadPhone === '') ? data['@inputCellPhoneRegister'] :this.functions.componentLogic.pipeMask.transform( data['@inputCellPhoneRegister'] , PipeMask.PHONE );
            const email = (isPostLoadEmail === '') ? data['@inputEmailRegister'] : this.functions.componentLogic.pipeMask.transform( data['@inputEmailRegister']  , PipeMask.EMAIL );
            this.functions.getComponentForId('@inputNameInsured').setValuePostLoad( data['@inputNameRegister'] );
            this.functions.getComponentForId('@inputLastNameInsured').setValuePostLoad( data['@inputLastNameRegister'] );
            this.functions.getComponentForId('@inputCellPhoneInsured').setValuePostLoad( cell );
            this.functions.getComponentForId('@inputEmailInsured').setValuePostLoad( email );
            if ( typePerson == 'r' ) {
                this.functions.getComponentForId('@inputInsuredNumberLicense').setValuePostLoad('');
            } else {
                this.functions.getComponentForId('@inputInsuredNumberLicense').setValuePostLoad( data['@inputNumberDocumentIdentity'] );
            }
            this.functions.getComponentForId('@inputRelationshipInsured').isInvisible();
        } else {
            this.functions.getComponentForId('@inputNameInsured').setValuePostLoad('');
            this.functions.getComponentForId('@inputLastNameInsured').setValuePostLoad('');
            this.functions.getComponentForId('@inputCellPhoneInsured').setValuePostLoad('');
            this.functions.getComponentForId('@inputEmailInsured').setValuePostLoad('');
            this.functions.getComponentForId('@inputInsuredNumberLicense').setValuePostLoad('');
            this.functions.getComponentForId('@inputRelationshipInsured').isVisible();
            this.functions.getComponentForId('@inputFrontDocumentLicenceInsured').isVisible();
            this.functions.getComponentForId('@inputBackDocumentLicenceInsured').isVisible();
        }
        const bool = this.functions.inspectionExpress();
        if (!bool) {
            this.functions.getComponentForId('@inputFrontDocumentLicenceInsured').isVisible();
            this.functions.getComponentForId('@inputBackDocumentLicenceInsured').isVisible();
        } else {
            this.functions.getComponentForId('@inputFrontDocumentLicenceInsured').isInvisible();
            this.functions.getComponentForId('@inputBackDocumentLicenceInsured').isInvisible();
        }
    }

    private showSectionThirdInformation(): void {
        const value = this.functions.componentLogic.data.getAllObject()['@inputDamageThird'];
        console.log('value', value);
        if (value=='Y') {
            this.functions.getComponentForId('@LabelPeopleInvolved').isVisible();
            this.functions.getComponentForId('@idCardThird').isVisible();
        } else {
            const valueIns = this.functions.componentLogic.data.getAllObject()['@inputPolicyAffected'];
            if(valueIns=='N'){
                this.functions.getComponentForId('@LabelPeopleInvolved').isVisible();
                this.functions.getComponentForId('@idCardThird').isVisible();
            }else{
                this.functions.getComponentForId('@LabelPeopleInvolved').isInvisible();
                this.functions.getComponentForId('@idCardThird').isInvisible();
            }
        }
    }

    private showSectionInjuredInformation(): void {
        const value = this.functions.componentLogic.data.getAllObject()['@inputDamageBody'];
        if (value=='Y') {
            this.functions.getComponentForId('@LabelPeopleInjured').isVisible();
            this.functions.getComponentForId('@idCardPeopleInjured').isVisible();
        } else {
            this.functions.getComponentForId('@LabelPeopleInjured').isInvisible();
            this.functions.getComponentForId('@idCardPeopleInjured').isInvisible();
        }
    }

    private showInspectionMethod(): void {
        const bool = this.functions.inspectionExpress();
        if (bool) {
            this.functions.getComponentForId('@labelSeccionInspeccionExpress').isVisible();
            this.functions.getComponentForId('@inputInspeccionExpress').isVisible();
        } else {
            this.functions.getComponentForId('@labelSeccionInspeccionExpress').isInvisible();
            this.functions.getComponentForId('@inputInspeccionExpress').isInvisible();
        }
    }

    public showDateInspectionExpress(component: IComponent): void {
        console.log('component.id' , component.id);
        const value = this.functions.getComponentForId(component.id).getValue();
        if ( value == 'C') {
            this.functions.getComponentForId('@inputDateInspectionExpress').isVisible();
            this.functions.getComponentForId('@inputHourInspectionExpress').isVisible();
        } else {
            this.functions.getComponentForId('@inputDateInspectionExpress').isInvisible();
            this.functions.getComponentForId('@inputHourInspectionExpress').isInvisible();
        }
    }

    public showDataForm(component: IComponent): void {
        const value = this.functions.getComponentForId(component.id).getValue();
        const data = this.functions.componentLogic.data.getAllObject();

        this.functions.getComponentForId('@inputNameReportComplete').isVisible();
        this.functions.getComponentForId('@inputLastNameReportComplete').isVisible();
        this.functions.getComponentForId('@inputCellPhoneReportComplete').isVisible();
        const complete = (data['@inputCellPhoneRegister_postLoad'] || '');
        if ( value == 'O') {
            this.functions.getComponentForId('@inputNameReportComplete').setValuePostLoad('');
            this.functions.getComponentForId('@inputLastNameReportComplete').setValuePostLoad('');
            this.functions.getComponentForId('@inputCellPhoneReportComplete').setValuePostLoad('');
        } else if ( value == 'A'){

            const cell = (complete !== '') ? this.functions.componentLogic.pipeMask.transform( data['@inputCellPhoneRegister'] , PipeMask.PHONE ) : data['@inputCellPhoneRegister'] ;
            this.functions.getComponentForId('@inputNameReportComplete').setValuePostLoad(data['@inputNameRegister']);
            this.functions.getComponentForId('@inputLastNameReportComplete').setValuePostLoad(data['@inputLastNameRegister']);
            this.functions.getComponentForId('@inputCellPhoneReportComplete').setValuePostLoad( cell );
        } else {
            this.functions.getComponentForId('@inputNameReportComplete').setValuePostLoad(data['@inputNameInsured']);
            this.functions.getComponentForId('@inputLastNameReportComplete').setValuePostLoad(data['@inputLastNameInsured']);
            this.functions.getComponentForId('@inputCellPhoneReportComplete').setValuePostLoad(data['@inputCellPhoneInsured']);
        }
    }

    public sendForm(): void {
        if ( this.functions.componentLogic.serviceForm.validateFormControl() ) {
            const authDocumentation = this.functions.getComponentForId('@inputCheckBoxAutorizationDocumentation').getValue();
            const authData = this.functions.getComponentForId('@inputCheckBoxAutorizationData').getValue();
            const authEnd = this.functions.getComponentForId('@inputCheckBoxAutorizationEnd').getValue();
            if ( authDocumentation === 'Y' && authData === 'Y' && authEnd === 'Y') {
                const modal = this.functions.componentLogic.modalService.open(ModalComponent , AppComponent.CENTER );
                modal.componentInstance.data = {
                    type : 'load',
                    value : 0
                };
                modal.result.then();
                const listThirdPartyInformation = this.getListThirdPartyInformation();
                let requestService : ISendData = {
                    createSinister: this.getCreateSinister(),
                    causeSinisterType : this.getCausaSiniestroType(),
                    detailsDriverInsuredVehicle: this.getDetailsDriverInsuredVehicle(),
                    informationClientNaturalPerson : this.getInformationClientNaturalRegistry(),
                    informationClientLegalPerson : this.getInformationClientLegalRegistry(),
                    listImagesSinister: this.getImages(),
                    listInjuredPerson: this.getListInjuredPerson(),
                    listThirdPartyInformation,
                    listWitnessesToTheAccident: this.getListWitnessesToTheAccident(),
                    lossType: this.getLossType(),
                    policyAffectedRamo: this.getpolicyAffectedRamo(),
                    sinisterCity: this.getCitySiniester(),
                    sinisterData: this.getSinisterData(listThirdPartyInformation),
                    vehicleData: this.getInformationCar()
                }
                console.log(JSON.stringify(requestService));
                const url = environment.host.concat(environment.path).concat(service.createSinister.path);
                this.functions.componentLogic.injectService.callServiceForUrl( service.createSinister.method , url , requestService).then(
                    (observer) => {
                        observer.subscribe(
                            (result : { code: number; message: string ; numberCreateSinister: string}) => {
                                console.log('Success' , result);
                                let url = '/complete';
                                if (result.code === 0) {
                                    url +='?status=ok&number='+result.numberCreateSinister; 
                                } else {
                                    url +='?status=pendiente'; 
                                }
                                modal.close();
                                sessionStorage.clear();
                                localStorage.clear();
                                sessionStorage.setItem('complete' , 'Y');
                                window.location.href = url;
                            }
                        );
                    }, this.errorSendService
                ).catch(this.errorSendService);
            } else {
                const modal = this.functions.componentLogic.modalService.open(ModalComponent , AppComponent.CENTER );
                modal.componentInstance.data = {
                    type : 'noConfirmAutorize',
                    value : 0
                };
                modal.result.then( (result) => { });
            }
        }
    } 

    private errorSendService(erno: any): void {
        console.log('Error service ' , erno);
        const modal = this.functions.componentLogic.modalService.open(ModalComponent , AppComponent.CENTER );
        modal.componentInstance.data = {
            type : 'ernoSendReport',
            value : 0
        };
        modal.result.then();
    }

    // Seccion datos del asegurado persona natural
    private getInformationClientNaturalRegistry(): IInformationClientNaturalPerson {
        const data = this.functions.componentLogic.data.getAllObject();
        const type = data['@inputTypeDocumentIdentity'];
        if ( type !== 'r') {
            let informationClientNaturalPerson: IInformationClientNaturalPerson = {
                cellPhone : this.getDataUserEncription().phone ,
                document : {
                    id : type,
                    number : data['@inputNumberDocumentIdentity'],
                    value: (type == 'c') ? 'Cédula' : 'Pasaporte'
                },
                email: this.getDataUserEncription().email ,
                id: (data['@inputHiddenIdCliente']) ? data['@inputHiddenIdCliente'] : '',
                landline: this.getDataUserEncription().landline ,
                lastName: data['@inputLastNameRegister'],
                name: data['@inputNameRegister']
            }
            return informationClientNaturalPerson;
        }
        return null;
    }

    // Seccion datos del asegurado persona natural
    private getInformationClientLegalRegistry(): IInformationClientLegalPerson {
        const data = this.functions.componentLogic.data.getAllObject();
        const type = data['@inputTypeDocumentIdentity'];
        if ( type === 'r') {
            let informationClientNaturalPerson: IInformationClientLegalPerson = {
                contactPerson : {
                    cellPhone : this.getDataUserEncription().phone ,
                    email: this.getDataUserEncription().email ,
                    landline : this.getDataUserEncription().landline,
                    lastName : data['@inputLastNameRegister'],
                    name : data['@inputNameRegister']
                },
                id: (data['@inputHiddenIdCliente']) ? data['@inputHiddenIdCliente'] : '',
                nameLegalPerson : data['@inputNameCompanyRegister'],
                numberRUC : {
                    id : 'r',
                    number : data['@inputNumberDocumentIdentity'],
                    value: 'RUC'
                }
            }
            return informationClientNaturalPerson;
        }
        return null;
    }

    // Seccion de datos del vehiculo
    private getInformationCar(): IVehicleData {

        const data = this.functions.componentLogic.data.getAllObject();
        // Modelo
        let model: any = {};
        const listModel = JSON.parse(data['@inputModelCarRegistry_options']);
        const modelId = data['@inputModelCarRegistry'];
        listModel.forEach( (modelItem) => {
            if (modelItem.key == modelId) {
                model = modelItem;
            }
        });
        // Si la carga de modelo no fue exitosa
        if (!model.key) {
            model = {
                key : modelId,
                value : modelId
            }
        }
        console.log('model' , model);
        // Marca
        let brand: any = {};
        const listBrand = JSON.parse(data['@inputBrandCarRegistry_options']);
        const brandId = data['@inputBrandCarRegistry'];
        listBrand.forEach( (brandItem) => {
            if (brandItem.key == brandId) {
                brand = brandItem;
            }
        });
        // Si la carga de marca no fue exitosa
        if (!brand.key) {
            brand = {
                key : brandId,
                value : brandId
            }
        }
        let vehicleData: IVehicleData = {
            agent : 'FNOL',
            brand : brand.value,
            brandId : brand.key,
            chassis : data['@inputNumberChasisCarRegistry2'],
            generatedSinisterToday: data['@inputHiddenGeneratedSinisterToday'],
            idModel : model.key,
            idPolicy : (data['@inputHiddenPolicyId']) ? data['@inputHiddenPolicyId']: '',
            idVehicle : (data['@inputHiddenVehicleId']) ? data['@inputHiddenVehicleId']: '',
            model : model.value,
            motor : data['@inputNumberMotorCarRegistry2'],
            plates : data['@inputNumberCarIdentityRegistry'],
            policyNumber : (data['@inputHiddenPolicyNumber']) ? data['@inputHiddenPolicyNumber']: 0,
            product : (data['@inputHiddenProduct']) ? data['@inputHiddenProduct']: '',
            productId : (data['@inputHiddenProductId']) ? data['@inputHiddenProductId']: '',
            validSince : (data['@inputHiddenValidSince']) ? data['@inputHiddenValidSince']: '',
            validUntil : (data['@inputHiddenValidUntil']) ? data['@inputHiddenValidUntil']: '',
            vehicleColor : data['@inputColorCarRegistry'],
            year : data['@inputYearCarRegistry']
        };
        return vehicleData;
    }

    // Seccion daños del siniestro / causa siniestro
    private getCausaSiniestroType() : ICauseSinisterType  {
        const data = this.functions.componentLogic.data.getAllObject();
        let dataResponse: any = {};
        const listSiniester = JSON.parse(data['@inputCauseSinister_options']);
        const modelId = data['@inputCauseSinister'];
        listSiniester.forEach( (item) => {
            if (item.key == modelId) {
                dataResponse = item;
            }
        });

        let causeSinisterType: ICauseSinisterType = {
            id: dataResponse.key,
            value: dataResponse.value
        }
        return causeSinisterType;
    }

    // Seccion póliza afectada
    private getpolicyAffectedRamo(): IPolicyAffectedRamo {
        let policyAffectedRamo: IPolicyAffectedRamo = {
            id : 'VH',
            value : 'Vehículos'
        }
        return policyAffectedRamo;
    }

    // Seccion daños del siniestro / Tipo perdida
    private getLossType() : ILossType {
        const data = this.functions.componentLogic.data.getAllObject();
        let dataResponse: any = {};
        const listTypeLost = JSON.parse(data['@inputTypeLost_options']);
        const type = data['@inputTypeLost'];
        listTypeLost.forEach( (item) => {
            if (item.key == type) {
                dataResponse = item;
            }
        });

        let lossType: ILossType = {
            id: dataResponse.key,
            value: dataResponse.value
        }
        return lossType;
    }

    // Seccion daños del siniestro / ciudad
    private getCitySiniester(): ISinisterCity {
        const data = this.functions.componentLogic.data.getAllObject();
        let dataResponse: any = {};
        const listCitySiniester = JSON.parse(data['@inputCityProcessSinister_options']);
        const city = data['@inputCityProcessSinister'];
        listCitySiniester.forEach( (item) => {
            if (item.key == city) {
                dataResponse = item;
            }
        });
        let sinisterCity: ISinisterCity = {
            id: dataResponse.key,
            value: dataResponse.value
        }
        return sinisterCity;
    }

    // Seccion daños del siniestro / data
    private getSinisterData( third?: IListThirdPartyInformation[]): ISinisterData{
        
        const data = this.functions.componentLogic.data.getAllObject();
        let thirdPartyDamage = (data['@inputDamageThird'] == 'Y') ? true : (third && third.length > 0);

        let sinisterData: ISinisterData = {
            canTheVehicleMobilizedByItsOwnMeans : (data['@inputMobilize'] == 'Y'),
            claimAddress : data['@inputAddressSinister'],
            coolantOilOrHydraulicFluidLeaked : ( data['@inputLiquidSinister'] == 'Y'),
            coordinateDamage : this.getCoordinateDamage(),
            howHappen: data['@inputDescriptionSinister'],
            // howManyPiecesWereAffectedInTheCrash: data['@inputCountPiece'],
            // Validar con Adrian estos dos Parametros
            wasAnyComplaintMade : (data['@inputComplaint'] == 'Y'),
            thereInjuriesOccupants : (data['@inputDamageBody'] == 'Y'),
            therePolicePresence : (data['@inputExistPresencePolice'] == 'Y'),
            // Validar hora
            timeHappened : Functions.getHourFormat( data['@inputHourSinister'] ),
            wasProformaRepairVehicleDamagesCarriedOut : (data['@inputProforma'] == 'Y'),
            whatDamages : data['@inputSinisterDetail'],
            // Validar Fecha
            whenHappen : data['@inputDateSinister'] ,
            whereHappen : this.getWhereHappen(),
            useTheContractedPolicyForRepair: (data['@inputPolicyAffected'] == 'Y'),
            thirdPartyDamage
        }
        return sinisterData;
    }

  // Seccion daños del siniestro / cuadrantes
    private getCoordinateDamage() : ICoordinateDamage {
        const data = this.functions.componentLogic.data.getAllObject();
        return this.getCoordinateDamageObject(
                data['@inputInteractiveBack'],
                data['@inputInteractiveFront'],
                data['@inputInteractiveRigth'],
                data['@inputInteractiveLeft']
                );
    }

    // Seccion daños del siniestro / ubicacion del siniestro
    private getWhereHappen() : IWhereHappen {
        return {
            cities : this.getCityHappen(),
            province : this.getProvinceHappen()
        }
    }

    // Seccion daños del siniestro / Ciudad del siniestro
    private getCityHappen() : ICities {
        const data = this.functions.componentLogic.data.getAllObject();
        let dataResponse: any = {};
        const listCity = JSON.parse(data['@inputCitySinister_options']);
        const city = data['@inputCitySinister'];
        listCity.forEach( (item) => {
            if (item.key == city) {
                dataResponse = item;
            }
        });

        let cityLocate: ICities = {
            id: city,
            value: dataResponse.value || ''
        }
        return cityLocate;
    }

    // Seccion daños del siniestro / provincia del siniestro
    private getProvinceHappen() : IProvince {
        const data = this.functions.componentLogic.data.getAllObject();
        let dataResponse: any = {};
        const listProvince = JSON.parse(data['@inputProvinceSinister_options']);
        const province = data['@inputProvinceSinister'];
        listProvince.forEach( (item) => {
            if (item.key == province) {
                dataResponse = item;
            }
        });

        let provinceLocate: IProvince = {
            id: dataResponse.key,
            value: dataResponse.value
        }
        return provinceLocate;
    }

    // Seccion datos del conductor
    private getDetailsDriverInsuredVehicle(): IDetailsDriverInsuredVehicle{

        const dateRegisterSiniester = new Date();
        const filingDate = dateRegisterSiniester.getFullYear() +'-'+ (dateRegisterSiniester.getMonth() + 1 ) +'-' +dateRegisterSiniester.getDate();
        const data = this.functions.componentLogic.data.getAllObject();
        const driverIsInsured =  (data['@inputInsuredDriverPage3'] == 'Y');
        let detailsDriverInsuredVehicle: IDetailsDriverInsuredVehicle = {
            addressOfYourHome : '',
            authorizedDriveBy : '',
            cellPhone : (driverIsInsured)? this.getDataUserEncription().phone :this.validateEncription( '@inputCellPhoneInsured' , data , 'P' )  ,
            dueDate : data['@inputInsuredDateLicense'],
            cityOfSettlementOfTheClaim: this.getCityClaim(),
            email : (driverIsInsured)? this.getDataUserEncription().email :this.validateEncription(  '@inputEmailInsured' , data , 'E'),
            filingDate,
            landline: (driverIsInsured)? this.getDataUserEncription().landline :'',
            lastNameDriver: data['@inputLastNameInsured'],
            licenseNumber: data['@inputInsuredNumberLicense'],
            licenseType: data['@inputInsuredTypeLicense'],
            namesDriver: data['@inputNameInsured'],
            relationshipWithTheInsured: ( data['@inputRelationshipInsured']) ? data['@inputRelationshipInsured'] : '',
            theDriverIsTheInsured:driverIsInsured,
            whoCompletedThisFormGroup : this.getWhoCompletedThisFormGroup()
        }
        return detailsDriverInsuredVehicle;
    }

    // Seccion datos del conductor / ciudad de radicacion del siniestro
    private getCityClaim() : string {
        const cacheData = this.functions.componentLogic.data.getAllObject();
        const dataCodeCitySiniester = cacheData['@inputProvinceSinister'];
        const arrayCityOptions = JSON.parse( cacheData['@inputProvinceSinister_options'] || '[]' );
        let dataLabelCitySiniester = '';
        arrayCityOptions.forEach((data) => {
            if (data.key === dataCodeCitySiniester) {
                dataLabelCitySiniester = data.value;
            }
        });
        return dataLabelCitySiniester;
    }

    // Seccion de Completar el formulario / Quien completo
    private getWhoCompletedThisFormGroup() : IWhoCompletedThisFormGroup {
        const data = this.functions.componentLogic.data.getAllObject();
        const inputSendReportComplete =  data['@inputSendReportComplete'];
        const driverIsInsured = (data['@inputInsuredDriverPage3'] == 'Y');
        const complete = (data['@inputHiddenAutomaticCompleteRegistry'] == 'Y');
        let cellPhone = '';
        // this.validateEncription('@inputCellPhoneReportComplete' , data , 'P') 
        switch (inputSendReportComplete) {
            case 'A':
                cellPhone = (complete) ? this.getDataUserEncription().phone :this.validateEncription( '@inputCellPhoneRegister' , data , 'P' )  ;
                break;
            case 'C':
                cellPhone = (driverIsInsured)? this.getDataUserEncription().phone :this.validateEncription( '@inputCellPhoneInsured' , data , 'P' ) ;
                break;
            default:
                cellPhone = Security.encription(data['@inputCellPhoneReportComplete']);
                break;
        }
        let responseFunction : IWhoCompletedThisFormGroup = {
            whoCompletedThisForm : data['@inputSendReportComplete'],
            cellPhone,
            lastName: data['@inputLastNameReportComplete'],
            name: data['@inputNameReportComplete']
        }
        return responseFunction;
    }

    // Seccion de Completar el formulario / Datos de creacion
    private getCreateSinister() : ICreateSinister  {
        
        const data = this.functions.componentLogic.data.getAllObject();
        let inspectionDate = '';
        if (data['@inputDateInspectionExpress'] && data['@inputDateInspectionExpress'] !== '') {
            const splitInspectionDate = data['@inputDateInspectionExpress'].split('-');
            // D/M/Y
            // A-M-D
            inspectionDate = splitInspectionDate[2] + '/';
            inspectionDate+= splitInspectionDate[1] + '/';
            inspectionDate+= splitInspectionDate[0];
        }
        
        let createSinister: ICreateSinister = {
            vehicleIsAffected: (data['@inputPolicyAffected'] == 'Y'),
            uuidRequest: data['UUID'],
            receiveACallFromTheAdvisor: (data['@inputInspeccionExpress'] == 'C'),
            theSinisterIsManual : !( data['@inputHiddenAutomaticCompleteDataVehicle'] == 'Y' && data['@inputHiddenAutomaticCompleteRegistry'] == 'Y' ),
            inspectionDate,
            inspectionHour: (data['@inputHourInspectionExpress'] && data['@inputHourInspectionExpress'] !== '') ? Functions.getHourFormat( data['@inputHourInspectionExpress'] ) : '',
            doAnIntrospection: (data['@inputInspeccionExpress'] != 'C' && data['@inputInspeccionExpress'] != '')
        }
        return createSinister;
    }

    // Seccion Heridos involucrados
    private getListInjuredPerson():IListInjuredPerson[] {
        const data = this.functions.componentLogic.data.getAllObject();
        let listInjuredPerson: IListInjuredPerson[] = [];
        const injured: any[] = (data['@idCardPeopleInjured']) ? JSON.parse(data['@idCardPeopleInjured']) : [];
        injured.forEach(
            (item) => {
                const injuredIsInsured =  ( item.info['@radioButtonWitness'] == 'Y') ;

                const injuredPerson : IListInjuredPerson = {
                    cellPhone : (injuredIsInsured) ? this.getDataUserEncription().phone : Security.encription(item.info['@inputCellPhoneRegisterWitness']),
                    email : (injuredIsInsured) ? this.getDataUserEncription().email : Security.encription(item.info['@inputEmailRegisterWitness']),
                    landline: (injuredIsInsured) ? this.getDataUserEncription().landline :'',
                    lastName: item.info['@inputLastNameRegisterWitness'],
                    name: item.info['@inputNameRegisterWitness'],
                    sayWhereYouWereAtTheTimeOfTheAccident:  item.info['@inputLocationCar'],
                    listIsTheInjuredPersonTheSameInsuredPerson : ( item.info['@radioButtonWitness'] == 'Y'),
                    relationshipWithTheInsured : ''
                }
                listInjuredPerson.push(injuredPerson);
            }
        );
            
        return listInjuredPerson;
    }

    // Seccion Testigos involucrados
    private getListWitnessesToTheAccident(): IListWitnessesToTheAccident[] {
        const data = this.functions.componentLogic.data.getAllObject();
        let listWitnessesToTheAccident:IListWitnessesToTheAccident[] = [];
        const witnesses: any[] = (data['@idCardWitnesses']) ? JSON.parse( data['@idCardWitnesses'] ) : [];
        witnesses.forEach(
            (item) => {
                const witnessesToTheAccident : IListWitnessesToTheAccident = {
                    name : item.info['@inputNameRegisterWitnesses'],
                    lastName: item.info['@inputLastNameRegisterWitnesses'],
                    cellPhone : Security.encription(item.info['@inputCellPhoneRegisterWitnesses']),
                    email :'',
                    landline : ''
                }
                listWitnessesToTheAccident.push(witnessesToTheAccident)
            }
        );
        return listWitnessesToTheAccident;
    }

    // Seccion Terceros involucrados
    private getListThirdPartyInformation(): IListThirdPartyInformation[] {
        const data = this.functions.componentLogic.data.getAllObject();
        let listThirdPartyInformation: IListThirdPartyInformation[] = [];
        this.imageThird = [];
        const third: any[] = (data['@idCardThird']) ? JSON.parse( data['@idCardThird'] ) : [];
        third.forEach(
            (item) => {
                const materialDamage = (item.info['inputArticleThird'] == 'm') ? 'Material' :  (item.info['inputArticleThird'] == 'v') ? 'Vehiculo' : 'Bien Inmueble';
                const valueDocument = (item.info['inputTypeDocumentThird'] == 'c') ? 'Cedula' :  (item.info['inputTypeDocumentThird'] == 'p') ? 'Pasaporte' : 'RUC';
                const documentThird: IDocument = {
                    id : item.info['inputTypeDocumentThird'],
                    number : item.info['inputNumberDocumentIdentityThird'],
                    value : valueDocument
                }


                const idMaterial = ( (item.info['inputArticleThird'] == 'm') ? '02' :  (item.info['inputArticleThird'] == 'v') ? '01' : '03' );

                const thirdPartyInformation: IListThirdPartyInformation = {
                    idTypeOfDamage: item.info['radioButtonThird'],
                    typeOfDamage: ( item.info['radioButtonThird'] == 'C' ) ? 'Corporal' : 'Material' ,
                    idMaterialDamage: ( item.info['radioButtonThird'] == 'M' ) ? idMaterial : '',
                    materialDamage: ( item.info['radioButtonThird'] == 'M' ) ? materialDamage : '',
                    document :documentThird,
                    name : item.info['inputNameRegisterThird'],
                    lastName : item.info['inputLastNameRegisterThird'],
                    lossAdress : '',
                    cellPhone :item.info['inputCellPhoneRegisterThird'],
                    email: item.info['inputEmailRegisterThird'],
                    chassis: item.info['inputNumberChasisCarThird'],
                    motor: item.info['inputNumberMotorCarThird'],
                    plates: item.info['inputNumberCarThird'],
                    descriptionWellAffected: item.info['inputDescriptionArticleThird'],
                    damageDescription: item.info['inputDescriptionSinister'],
                    nameOfTheInsurer: ( item.info['inputNameArticleInsuredThird'] ) ? item.info['inputNameArticleInsuredThird'] : '',
                    theAffectedPropertyIsInsured : ( item.info['radioArticleInsuredThird'] == 'Y') ,
                    coordinateDamage: this.getCoordinateDamageObject(
                        item.info['inputInteractiveBackThird'],
                        item.info['inputInteractiveFrontThird'],
                        item.info['inputInteractiveLeftThird'],
                        item.info['inputInteractiveRigthThird']
                    ),    
                }
                listThirdPartyInformation.push(thirdPartyInformation);
                let tmpRouteImage = '';
                tmpRouteImage = (item.info['inputFrontDocumentRegisterThird']) ? item.info['inputFrontDocumentRegisterThird'] : '' ;
                if (tmpRouteImage !== '') this.imageThird.push(tmpRouteImage);
                
                tmpRouteImage = (item.info['inputBackDocumentRegisterThird']) ? item.info['inputBackDocumentRegisterThird'] : '' ;
                if (tmpRouteImage !== '') this.imageThird.push(tmpRouteImage);
                
                tmpRouteImage = (item.info['inputPhotoFrontLicenseThird']) ? item.info['inputPhotoFrontLicenseThird'] : '' ;
                if (tmpRouteImage !== '') this.imageThird.push(tmpRouteImage);
                
                tmpRouteImage = (item.info['inputPhotoBackLicenseThird']) ? item.info['inputPhotoBackLicenseThird'] : '' ;
                if (tmpRouteImage !== '') this.imageThird.push(tmpRouteImage);
            }
        );
        return listThirdPartyInformation;
    }

    // Seleccion de cuadrantes
    private getCoordinateDamageObject( backInput: string ,frontInput: string , leftInput: string , rightInput: string ):ICoordinateDamage {
        const back = ( backInput) ? backInput.split(',') : [] ;
        const front = ( frontInput) ? frontInput.split(',') : [] ;
        const left = ( leftInput) ? leftInput.split(',') : [] ;
        const right = ( rightInput) ? rightInput.split(',') : [] ;
        let coordinate: ICoordinateDamage = {
            back : Functions.validateQuadrant(back),
            front : Functions.validateQuadrant(front),
            left : Functions.validateQuadrant(left),
            right : Functions.validateQuadrant(right),
        }
        return coordinate;
    }  

    // Seccion Imagenes 
    private getImages(): string[] {
        const data = this.functions.componentLogic.data.getAllObject();
        let images: string[] = [];
        let tmpRouteImage = '';

        //imagen denuncia
        tmpRouteImage = (data['@inputDamageSinister']) ? data['@inputDamageSinister'] : '';
        if (tmpRouteImage !== '') images.push(tmpRouteImage.replace('//uploads',''));

        //imagen reporte policial
        tmpRouteImage = (data['@inputDocumentReportPolice']) ? data['@inputDocumentReportPolice'] : '';
        if (tmpRouteImage !== '') images.push(tmpRouteImage.replace('//uploads',''));

        //imagen proforma
        tmpRouteImage = (data['@inputPhotoProforma']) ? data['@inputPhotoProforma'] : '';
        if (tmpRouteImage !== '') images.push(tmpRouteImage.replace('//uploads',''));

        //imagen frontal licencia
        tmpRouteImage = (data['@inputFrontDocumentLicenceInsured']) ? data['@inputFrontDocumentLicenceInsured'] : '';
        if (tmpRouteImage !== '') images.push(tmpRouteImage.replace('//uploads',''));

        //imagen posterior licencia
        tmpRouteImage = (data['@inputBackDocumentLicenceInsured']) ? data['@inputBackDocumentLicenceInsured'] : '';
        if (tmpRouteImage !== '') images.push(tmpRouteImage.replace('//uploads',''));

        //imagen frontal documento identidad
        tmpRouteImage = (data['@inputFrontDocumentRegister']) ? data['@inputFrontDocumentRegister'] : '';
        if (tmpRouteImage !== '') images.push(tmpRouteImage.replace('//uploads',''));

        //imagen posterior documento identidad
        tmpRouteImage = (data['@inputBackDocumentRegister']) ? data['@inputBackDocumentRegister'] : '';
        if (tmpRouteImage !== '') images.push(tmpRouteImage.replace('//uploads',''));

        //imagen frontal matricula del vehiculo
        tmpRouteImage = (data['@inputNumberCarVehicleFront']) ? data['@inputNumberCarVehicleFront'] : '';
        if (tmpRouteImage !== '') images.push(tmpRouteImage.replace('//uploads',''));

        //imagen posterior matricula del vehiculo
        tmpRouteImage = (data['@inputNumberCarVehicleBack']) ? data['@inputNumberCarVehicleBack'] : '';
        if (tmpRouteImage !== '') images.push(tmpRouteImage.replace('//uploads',''));

        console.log('this.imageThird ' , this.imageThird);

        this.imageThird.forEach(
            (item) => {
                images.push(item.replace('//uploads',''));
            }
        );

        return images;
    }

    // Validacion ecripcion
    private validateEncription( id: string , data: any , type : 'E' | 'P') : string{
        let valueResponse = '';
        const value = data[id];
        if ( type === 'E') {
            valueResponse = (value.indexOf('******@') > -1) ? this.getDataEncriptUser(type , data) :  Security.encription(value) ;
        } else if ( type === 'P') {
            valueResponse = ( value.indexOf('******') > -1) ? this.getDataEncriptUser(type , data) :  Security.encription(value) ;
        }
        return valueResponse;
    }

    private getDataEncriptUser( type: string  , data: any) : string {
        if(type == 'P') {
            return data['@inputCellPhoneRegister']
        }
        return data['@inputEmailRegister'];
    }

    // Obtiene el objeto email y phone para encripcion
    private getDataUserEncription(): { email: string , phone: string , landline : string} {
        let email = '';
        let phone = '';
        let landline = '';

        const isPostLoadPhone = localStorage.getItem('@inputCellPhoneRegister_postLoad') || '';
        const isPostLoadLandline = localStorage.getItem('@inputPhoneRegister_postLoad') || '';
        const isPostLoadEmail = localStorage.getItem('@inputEmailRegister_postLoad') || '';

        if (isPostLoadPhone === '' ) {
            const phoneDecription = Number(localStorage.getItem('@inputCellPhoneRegister'));
            phone = Security.encription( '' + phoneDecription );
        } else {
            const phoneDecrip = Security.decription( localStorage.getItem('@inputCellPhoneRegister') ) ;
            const clearData = Functions.cleanAllArray(phoneDecrip.split(''));
            const phoneDecription = Number( clearData.join('') );
            phone = Security.encription( '' + phoneDecription );
        }
        if (isPostLoadLandline === '' ){
            landline = Security.encription( localStorage.getItem('@inputPhoneRegister') );
        }else {
            landline = localStorage.getItem('@inputPhoneRegister') ;
        }
        if (isPostLoadEmail === '' ){
            email = Security.encription( localStorage.getItem('@inputEmailRegister') );
        }else {
            email = localStorage.getItem('@inputEmailRegister') ;
        }

        return {
            email,
            phone,
            landline
        }
    }
}   