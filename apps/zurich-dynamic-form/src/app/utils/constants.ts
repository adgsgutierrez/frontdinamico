export const APPLICATION = {
    REQUEST : {
        FORM : '/assets/form.configuration.json'
    },
    MONTHS : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']
}