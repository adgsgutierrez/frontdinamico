import { Routes } from "@angular/router";
import { AppComponent } from "./app.component";
import { ComponentLogicComponent } from "./components/component-logic/component-logic.component";
import { PageResultComponent } from "./components/page-result/page-result.component";

export const routes: Routes = [
    { path: '', component: AppComponent },
    { path: 'form/:step', component: ComponentLogicComponent },
    { path: 'complete' , component: PageResultComponent }
  ];