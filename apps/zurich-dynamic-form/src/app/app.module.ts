import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ComponentLogicComponent } from './components/component-logic/component-logic.component';
import { UiModule } from '@zurich-dynamic-form/ui'; 
import { FormService, HttpGeneralService , HttpServiceService , HttpInterceptorService } from '@zurich-dynamic-form/services';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PageResultComponent } from './components/page-result/page-result.component';
import { PipeMask } from 'libs/ui/src/lib/input-mask/Pipes/PipeMask.transform';

@NgModule({
  declarations: [AppComponent, ComponentLogicComponent, PageResultComponent],
  imports: [HttpClientModule , BrowserModule, AppRoutingModule , UiModule, NgbModule , ReactiveFormsModule , FormsModule ],
  providers: [
    PipeMask,
    HttpGeneralService , 
    HttpServiceService , 
    FormService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
