import { HttpHeaders } from '@angular/common/http';

export interface SchemaHttp {
    url: string;
    body: any;
    headers?: any;
}

export interface SchemaToken {
    access_token: string;
    scope: string;
    token_type: string;
    expires_in: number;
}

export interface SchemaCentralCall {
    method: string;
    path: string;
}