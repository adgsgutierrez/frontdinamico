export interface IHelp{
    title: string;
    description: string;
    image: string;
    options: IHelpOption[];
}

export interface IHelpOption{
    img: string ;
    label: string;
    sublabel: string;
}

export interface IOptions{
    key?: string;
    value?: string;
    title?: string;
    description?: string;
}

export interface IDisable {
    max?: 'now' | 'day' | 'month' | 'year';
    min?: 'now' | 'day' | 'month' | 'year';
    habilityText?: boolean;
    weekend: boolean;
    work: boolean;
}

export interface ITransform {
    uppercase?: boolean;
    lowercase?: boolean;
}

export interface IExtra{
    visible?: boolean;
    type?: string;
    edit?: boolean;
    title: string;
    subTitle: string;
    help?: IHelp;
    method?: string;
    placeholder?:string;
    disable?: IDisable;
    placeholderMobile?:string;
    placeholderDesktop?:string;
    options?: IOptions[];
    idService?: string;
    idSaveLocal: string;
    urlImage?: string;
    validators?: string[];
    messageInformation?: string;
    transform?: ITransform;
}

export interface IComponent{
    id: string;
    type: string;
    urlPreload?: string;
    urlPostChange?: string;
    autoUrl?: string;
    extras: IExtra;
}

export interface ITemplate{
    components: IComponent[]
}

export interface IStep{
    index: number;
    currentStep: string[];
}

export interface IFormConfiguration{
    headers : {
        showSteps : boolean;
    };
    steps: IStep[];
    templates: any;
}