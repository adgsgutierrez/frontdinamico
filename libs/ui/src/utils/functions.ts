import { IComponent } from "@zurich-dynamic-form/schemas";
import { DeviceDetectorService } from 'ngx-device-detector';

export class Functions{

    public static EXTENSIONS = {
        img : ['jpg' , 'jpeg' , 'bmp' ,  'png' , 'gif' ],
        pdf : ['pdf'],
        xls : ['xlsx' ,'xlsm' ,'xlsb' ,'xltx' ,'xltm' ,'xls' ,'xlt' ,'xls' ,'xml' ,'xlam' ,'xla' ,'xlw' ,'xlr'],
        txt : ['txt'],
        csv : ['csv'],
        html : ['html'],
        video : ['webm','mkv','flv','flv','vob','ogv','ogg','drc','gif','gifv','mng','avi','MTS','M2TS','TS','mov','qt','wmv','yuv','rm','rmvb','viv','asf','amv','mp4','m4p','m4v','mpg ','mp2','mpeg','mpe','mpv','mpg','mpeg','m2v','m4v','svi','3gp','3g2','mxf','roq','nsv','flv ','f4v ','f4p ','f4a ','f4b'],
        audio : ['3gp','aa','aac','aax','act','aiff','alac','amr','ape','au','awb','dss','dvf','flac','gsm','iklax','ivs','m4a','m4b','m4p','mmf','mp3','mpc','msv','nmf','ogg','oga','mogg','opus','org','ra ','rm','raw','rf64','sln','tta','voc','vox','wav','wma','wv','webm','8svx','cda'],
    }

    public static getPlaceHolder(component: IComponent , deviceDetector: DeviceDetectorService): string{
        let placeholder = '';
        if (component.extras.placeholder && component.extras.placeholder !== '') {
            placeholder = component.extras.placeholder;
        } else {
            if (component.extras.placeholderDesktop && component.extras.placeholderMobile){
                if (deviceDetector.isDesktop()){
                    placeholder = component.extras.placeholderDesktop;
                } else if (deviceDetector.isMobile() || deviceDetector.isTablet()){
                    placeholder = component.extras.placeholderMobile;
                } else {
                    placeholder = '';
                }
            } else {
                placeholder = '';
            }
        }
        return placeholder;
    }

    public static validateEmail( value: string): boolean {
        const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(value);
    }

    public static validateRegex( regex: string , value: string): boolean{
        let reg =  new RegExp( regex );
        let val = reg.test(value);
        return (value !== '' ) ? val : true;
    }

    public static validateExtension(nameFile: string , type: string[]) : boolean {
        const arraySplit = nameFile.split('.');
        const extension = arraySplit[ (arraySplit.length - 1) ] || '';
        if (extension !== '' && (type.length > 0)) {
            if (type.indexOf('all') > -1) {
                return true;
            }
            let evalueKey = false;
            type.forEach(
                (typeEvaluate) => {
                    const values = Functions.EXTENSIONS[typeEvaluate];
                    if (values.indexOf(extension) > -1){
                        evalueKey = true;
                    }
                }
            );
            return evalueKey;
        }
        return false;
    }

    public static cleanArray( actual: string [] ): string[]{
        var newArray = new Array();
        for( var i = 0, j = actual.length; i < j; i++ ){
            let value = actual[ i ] || ''
            value = value.replace('\n','');
            value = value.replace('\r','');
            value = value.replace('\t','');
            value = value.trim();
            if ( value !== '' && value.charCodeAt(0) > 20){
              newArray.push( actual[ i ] );
          }
        }
        return newArray;
    }

}