import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { IComponent, IOptions } from '@zurich-dynamic-form/schemas';
import { HttpServiceService } from '@zurich-dynamic-form/services';
import { Transversal } from '../ui.transversal';
declare var $:any;
@Component({
  selector: 'zurich-dynamic-form-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class Select extends Transversal {

  public title: string;
  public subtitle: string;
  public placeholder: string = 'Selecciona una opción';
  public options: IOptions[];
  public id: string;
  public filterValue: string;
  public optionsTmp: IOptions[] = [];
  public isPlaceholder: boolean = true;

  constructor(private changeDetection: ChangeDetectorRef) {
    super();
   }
  
   onPostInit(dataSelect: string): void {
    this.title = this.component.extras.title;
    this.subtitle = this.component.extras.subTitle;
    this.placeholder = ( this.component.extras.placeholder ) ? this.component.extras.placeholder : this.placeholder;
    this.isPlaceholder = true;
    const isEditableField = localStorage.getItem(this.component.id + '_editable') || '';

    if (isEditableField === '' ) {

      if (this.component.extras.options && this.component.extras.options.length > 0 ){
        this.setOptions(this.component.extras.options);
      } else {
        this.setOptions(JSON.parse( localStorage.getItem(this.component.id + '_options') || '[]' ));
      }

      this.id = this.component.id;

      if (dataSelect !== '' && dataSelect !== null) {
        this.component.extras.options.forEach(
          (option) => {
            if ( ('' + option.key) == ('' + dataSelect) ) {
              this.isPlaceholder = false;
              this.placeholder = option.value;
              this.value = option.key;
            }
          }
        );
      }
    } else {
      this.isChangeService = true;
    }
  }

  setOptions(options: IOptions[]): void{
    // Orden Asendente
    this.component.extras.options = options.sort((a, b) => (a.value < b.value ? -1 : 1));;
    this.isPlaceholder = true;
    this.optionsTmp = options;
    localStorage.setItem(this.component.id + '_options' , JSON.stringify(this.optionsTmp));
    this.changeDetection.detectChanges();
  }

  filterOptions(): void {
    this.component.extras.options = this.optionsTmp.filter(
      (option) =>{ return (option.value.toLowerCase().indexOf(this.filterValue.toLowerCase()) > -1  ) }
    );
  }

  select(option: IOptions): void {
    this.filterValue = '';
    this.placeholder = option.value;
    this.component.extras.options = this.optionsTmp;
    console.log('this.component.extras.options' , this.component.extras.options);
    this.isPlaceholder = false;
    this.setValue(option.key);
    this.isNextFunction();
  }

  setValueInput(value : string ): void {
    this.isPlaceholder = false;
    this.setValue(value);
  }

}
