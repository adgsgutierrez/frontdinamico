import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { PageComponent } from './page/page.component';

const routes: Routes = [
  {
    path : '',
    component: IndexComponent,
  },
  {
    path : 'newInformation',
    component: PageComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddcardPopupRoutingModule { }
