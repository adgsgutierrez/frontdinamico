import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ITransversal } from '../ui.itransversal';

import { Steps } from './steps.component';

describe('Steps', () => {
  let component: ITransversal;
  let fixture: ComponentFixture<ITransversal>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Steps ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Steps);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
