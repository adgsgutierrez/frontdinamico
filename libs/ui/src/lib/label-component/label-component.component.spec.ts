import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ITransversal } from '../ui.itransversal';

import { Label } from './label-component.component';

describe('Label', () => {
  let component: ITransversal;
  let fixture: ComponentFixture<ITransversal>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Label ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Label);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
