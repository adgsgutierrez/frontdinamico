import { Data } from '@zurich-dynamic-form/data';
import { IComponent, IFormGroup, IOptions } from '@zurich-dynamic-form/schemas'
import { HttpServiceService } from '@zurich-dynamic-form/services';
import { Observable } from 'rxjs';

export interface ITransversal{

    setClass(classBackground: string): void;

    onPostInit(dataSelect: string): void;

    setDataComponent(component: IComponent): void;

    setValue(value: string): void;

    setValuePostLoad(value: string): void;

    setLabel(label: string): void;

    setSubLabel(sublabel: string) : void;

    isVisible() : void;

    isInvisible() : void;

    setDataBehavior(data: Data): void;

    setService(service:HttpServiceService): void;

    sendEmiterEvent(): Observable<any>;

    getComponent(): IComponent;

    getValue(): string;

    isValid(focus?:boolean): boolean;

    searchInformation(): void;

    isErrorService() : boolean ;

    setVisibleForError(flag: boolean): void ;

    setOptions(option: IOptions[]): void;

    setIsChangeService(fail: boolean) : void ;

    getIsItemVisible(): boolean;
}