import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideTooltipComponent } from './slide-tooltip.component';

describe('SlideTooltipComponent', () => {
  let component: SlideTooltipComponent;
  let fixture: ComponentFixture<SlideTooltipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SlideTooltipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
