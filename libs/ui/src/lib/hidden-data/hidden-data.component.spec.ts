import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ITransversal } from '../ui.itransversal';

import { HiddenData } from './hidden-data.component';

describe('HiddenData', () => {
  let component: ITransversal;
  let fixture: ComponentFixture<ITransversal>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HiddenData ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HiddenData);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
