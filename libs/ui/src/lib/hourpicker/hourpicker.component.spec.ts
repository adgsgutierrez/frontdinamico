import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ITransversal } from '../ui.itransversal';

import { Hourpicker } from './hourpicker.component';

describe('Hourpicker', () => {
  let component: ITransversal;
  let fixture: ComponentFixture<ITransversal>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hourpicker ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hourpicker);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
