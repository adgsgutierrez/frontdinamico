import { Component, OnInit } from '@angular/core';
import { NgbDropdownConfig, NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { Transversal } from '../ui.transversal';

@Component({
  selector: 'zurich-dynamic-form-hourpicker',
  templateUrl: './hourpicker.component.html',
  styleUrls: ['./hourpicker.component.scss'],
  providers: [NgbDropdownConfig , NgbTimepickerConfig]
})
export class Hourpicker extends Transversal{

  public id: string;
  public time = {hour: 13, minute: 30};
  public meridian = true;
  public title: string;
  public subtitle: string;
  public msmErno = '';

  constructor(private config: NgbTimepickerConfig) { 
    super();
    this.config.meridian = this.meridian;
    this.config.spinners =  true;
    this.config.seconds =  false;
    this.config.hourStep =  1;
    this.config.minuteStep =  1;
    this.config.secondStep =  1;
    this.config.disabled =  false;
    this.config.readonlyInputs =  false;
    this.config.size = 'medium';
  }

  setOptions(options: any[]): void{}

  onPostInit(dataSelect: string): void {
    this.id = this.component.id;
    this.title = this.component.extras.title;
    this.subtitle =this.component.extras.subTitle;
    this.id = this.component.id;
    const data = new Date();
    dataSelect = (dataSelect) ? dataSelect : data.getHours()+':'+data.getMinutes();
    const valueHour = dataSelect.split(':');
    this.time = {
      hour: Number(valueHour['0']),
      minute:Number(valueHour[1])
    }
    if (this.component.extras.edit) {
      if (this.time.minute !== 0 && this.time.minute !== 30){
        this.time.minute = 0;
      }
      this.config.minuteStep = 30;
      this.config.readonlyInputs =  true;
    }

    this.value = ((this.time.hour > 9 )?
                        this.time.hour: 
                        '0'+this.time.hour) 
                        +':'+ 
                  ((this.time.minute>9)?
                        this.time.minute: 
                        '0'+ this.time.minute);
  }

  toggleMeridian() {
      this.meridian = !this.meridian;
  }

  setHour(myDrop){
    this.setValue( ((this.time.hour > 9 )?this.time.hour: '0'+this.time.hour) +':'+ ((this.time.minute>9)?this.time.minute: '0'+ this.time.minute) );
    
    myDrop.close();
  }

  valideHour(): void {
    
    if (this.component.extras && this.component.extras.disable) {
      if (this.component.extras.disable.work) {
        if (this.time.hour >= 0 && this.time.hour < 8  ){
          this.msmErno = 'En este horario no exite atención disponible';
        } else if (this.time.hour > 12 && this.time.hour < 14  ){
          this.msmErno = 'En este horario no exite atención disponible';
        }else if (this.time.hour > 18 && this.time.hour <= 24  ){
          this.msmErno = 'En este horario no exite atención disponible';
        }else {
          this.msmErno = '';
        }
      } 
    } 
  }
}

