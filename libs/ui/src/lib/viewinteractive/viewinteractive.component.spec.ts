import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ITransversal } from '../ui.itransversal';

import { Viewinteractive } from './viewinteractive.component';

describe('Viewinteractive', () => {
  let component: ITransversal;
  let fixture: ComponentFixture<ITransversal>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Viewinteractive ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Viewinteractive);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
