import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TooltipInformationComponent } from './tooltip-information.component';


jest.mock(
  'jquery',
  () => {
    return jest.fn(() => {
      popover: jest.fn()
    });
  }
);


const mockTitle = 'Title test';
const mockImage = 'assets/img/test.png';
const mockBody = 'Body test';

describe('TooltipInformationComponent', () => {
  let component: TooltipInformationComponent;
  let fixture: ComponentFixture<TooltipInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TooltipInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TooltipInformationComponent);
    component = fixture.componentInstance;
    component.title = mockTitle;
    component.body = mockBody;
    component.img = mockImage;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component.contentTooltip()).not.toBeNaN(); 
  });
});
