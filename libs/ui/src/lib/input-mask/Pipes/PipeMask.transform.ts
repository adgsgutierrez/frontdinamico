import { Pipe, PipeTransform } from '@angular/core';
import { Functions } from 'libs/ui/src/utils/functions';
import { Security } from 'libs/ui/src/utils/security';

@Pipe({ name: 'maskcrypt' })
export class PipeMask implements PipeTransform {
  
    public static PHONE = 'phone';
    public static EMAIL = 'email';

    transform( value: string , type: string): string {
        if (value !== ''){
            let val = Security.decription( value );
            const data = Functions.cleanArray(val.split(''));
            // valida cantidad para enmascarar
            if (data.length > 5) {
                console.log(` -${data}-`);
                // Encripcion Phone
                if (type === PipeMask.PHONE) {
                    let dataReturn = '';
                    for ( let i = 0 ; i < 3 ; i++) {
                        dataReturn += data[i];
                    }
                    dataReturn += '****';
                    for (let i = (data.length - 3) ; i < data.length ; i++) {
                        dataReturn += data[i];
                    }
                    return dataReturn;
                } 
                // Encripcion Email
                else if ( type === PipeMask.EMAIL) {
                    const partFront = val.split('@');
                    const valueFront = partFront[0];
                    let dataReturn = '';
                    for ( let i = 0 ; i < 3 ; i++) {
                        dataReturn += data[i];
                    }
                    dataReturn += '******@';
                    dataReturn += partFront[1];
                    return dataReturn;

                } 
                // Encripcion Any
                else {
                    let dataReturn = '******';
                    for ( let i = (data.length-6) ; i < data.length ; i++) {
                        dataReturn += data[i];
                    }
                    return dataReturn;
                }
            }
            return val;
        }
        return value;
    }


}