import 'zone.js/dist/zone-testing';
import { getTestBed } from '@angular/core/testing';
import {  inject, TestBed } from '@angular/core/testing';
import { Components, ITransversal } from '@zurich-dynamic-form/ui';
import { Title } from 'libs/ui/src/lib/title/title.component';
import { Data } from './data';

describe('Unit Test Data', () => {


  beforeEach(( done ) => {
    TestBed.configureTestingModule({
      declarations: [ Title ]
    })
    done();
  });


  it('Set Component Transversal' , inject([Title] , (component : ITransversal) => {
    // state initial
    expect(0).toBe( 
      Data.getInstance().getComponents().length
    );
    // add component
    component.setDataComponent({
      "id": "labelSection",
      "type": "Title",
      "extras": {
          "visible" : false,
          "title": "Title for label",
          "subTitle": "Subtitle for lable",
          "idSaveLocal" : "label"
      }
    });
    Data.getInstance().setComponentBehavior(component);

    // state end
    expect(1).toBe( 
      Data.getInstance().getComponents().length
    );

  }));
});
