import { ITransversal } from '@zurich-dynamic-form/ui';
import { Observable } from 'rxjs';

export interface IData{

    setComponentBehavior(component: ITransversal): void;

    getDataForBehavior():void;

    setKeyValue(key: string, value: string): void;
}