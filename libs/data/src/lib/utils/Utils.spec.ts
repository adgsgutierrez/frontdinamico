import { Utils } from './Utils';

describe('Unit Test Utils', () => {
  it('Search Param in Object', ( done ) => {
    const paramToSearch = 'vehicle';
    const objectToFind = {
        home: {
            address : 'Av 123',
            door : 5,
            parking : {
                vehicle : 'moto'
            }
        },
        office : {
            address : 'Av 125',
            door : 25
        }
    };
    expect('moto').toBe( Utils.searchParam(objectToFind , paramToSearch) );
    done();
  });

  it('Search Param not found in Object', ( done ) => {
    const paramToSearch = 'public';
    const objectToFind = {
        home: {
            address : 'Av 123',
            door : 5,
            parking : {
                vehicle : 'moto'
            }
        },
        office : {
            address : 'Av 125',
            door : 25
        }
    };
    expect('').toBe( Utils.searchParam(objectToFind , paramToSearch) );
    done();
  });

});
