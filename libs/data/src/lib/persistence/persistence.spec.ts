import { Persistence } from './persistence';

describe('Unit Test Persistence', () => {

  const persistence = new Persistence();
  it('Save and Read Object In Localstorage', ( done ) => {
    const dataSaveLocal = 'data saved';
    const objectSave = {
        data : dataSaveLocal
    };
    persistence.save(objectSave);
    expect(dataSaveLocal).toBe(persistence.read('data'));
    done();
  });

  it('Remove Object In Localstorage', ( done ) => {
    const dataSaveLocal = 'data saved';
    const objectSave = {
        data : dataSaveLocal
    };
    persistence.save(objectSave);
    expect(dataSaveLocal).toBe(persistence.read('data'));
    persistence.removeElement('data');
    const read = persistence.read('data') || '';
    expect('').toBe(read);
    done();
  });

});