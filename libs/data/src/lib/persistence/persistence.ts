export class Persistence {
    
    public save(object: any): void{
        const keys: string[] = Object.keys(object);
        for (let key of keys) {
            localStorage.setItem(key , (object[key] !== null) ? object[key] : '' );
        }
    }

    public read(key: string): string{
        return localStorage.getItem(key) || '';
    }

    public removeElement(key: string) : void {
        localStorage.removeItem(key);
    }
}