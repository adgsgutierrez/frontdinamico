/**
 * Copyright (c) everis and NTT Data. All rights reserved.
 * Licensed under the MIT License.
 * Zurich Dymanic Form
 * Version: 1.0.0
 * Date: 2020
 * Author: everis Aric Gutierrez
 * All rights reserved
 * @description Class in charge of test to http-service.service
 */

import { HttpGeneralService } from './http-service.service';
import { SchemaHttp } from '@zurich-dynamic-form/schemas';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';

describe('HttpGeneralService', (): void => {

  let service: HttpGeneralService;
  let injectionClassDummy;

  class InjectionClassDummy {
    public get(url: string, options?: any ): Observable<Object> {
      return of(dummyResponse);
    }
    public post(url: string, body: any | null, options?: any): Observable<Object> {
      return of(dummyResponse);
    }
    public put(url: string, body: any | null, options?: any): Observable<Object> {
      return of(dummyResponse);
    }
    public patch(url: string, body: any | null, options?: any): Observable<Object> {
      return of(dummyResponse);
    }
  }

  const dummyRequest: SchemaHttp = {
    url: '/services/service', body: {}, headers: new HttpHeaders()
  };
  const dummyResponse: any = {
    code: 200, data: {}, sms: 'Data response'
  };

  beforeEach(() => {
    injectionClassDummy = new InjectionClassDummy();
    service = new HttpGeneralService(injectionClassDummy as HttpClient);
  });

  afterEach(()=>{
    injectionClassDummy = null;
    service = null;
  })

  it('Call Service Get', (done): void => {
    service.methodGet(dummyRequest).subscribe((result) => {
      expect(result).toBe(dummyResponse);
      
      done();
    });
  });

  it('Call Service Post', (done): void => {
    service.methodPost(dummyRequest).subscribe((result) => {
      expect(result).toBe(dummyResponse);
      done();
    });
  });
  
  it('Call Service Put', (done): void => {
    service.methodPut(dummyRequest).subscribe((result) => {
      expect(result).toBe(dummyResponse);
      done();
    });
  });
  
  it('Call Service Patch', (done): void => {
    service.methodPatch(dummyRequest).subscribe((result) => {
      expect(result).toBe(dummyResponse);
      done();
    });
  });

});
