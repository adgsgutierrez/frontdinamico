/**
 * Copyright (c) everis and NTT Data. All rights reserved.
 * Licensed under the MIT License.
 * Zurich Dymanic Form
 * Version: 1.0.0
 * Date: 2020
 * Author: everis Aric Gutierrez
 * All rights reserved
 * @description Class in charge of http communication
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { SchemaHttp } from '@zurich-dynamic-form/schemas'
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpGeneralService {

  constructor(private httpclient : HttpClient) { }

  /**
   * @description Method to call service get
   * @param sendThisData Object to data from send at service 
   * @returns Observable with response
   */
  public methodGet(sendThisData: SchemaHttp): Observable<any>{
    return this.httpclient.get( sendThisData.url , {
      headers: sendThisData.headers,
      reportProgress : true
  });
  }
  /**
   * @description Method to call service get
   * @param sendThisData Object to data from send at service 
   * @returns Observable with response
   */
  public methodGetPromise(sendThisData: SchemaHttp): Promise<any>{
    return new Promise( (resolve , reject)=> {
      this.httpclient.get( sendThisData.url ,{ headers: sendThisData.headers })
      .pipe(
        catchError((err) => {
          console.error(err);
          return throwError(err);
        })
      ).subscribe(
        (data) => {resolve(data)} , (err) => { reject(err)});
      });
  }
  /**
   * @description Method to call service post
   * @param sendThisData Object to data from send at service 
   * @returns Observable with response
   */
  public methodPost(sendThisData: SchemaHttp): Observable<any>{
    return this.httpclient
    .post( sendThisData.url , sendThisData.body ,{ headers: sendThisData.headers })
  }
  /**
   * @description Method to call service put
   * @param sendThisData Object to data from send at service 
   * @returns Observable with response
   */
  public methodPut(sendThisData: SchemaHttp): Observable<any>{
    return this.httpclient.put( sendThisData.url , sendThisData.body ,{ headers: sendThisData.headers })
    .pipe(
      catchError((err) => {
        console.error(err);
        return throwError(err);
      })
    );
  }
  /**
   * @description Method to call service patch
   * @param sendThisData Object to data from send at service 
   * @returns Observable with response
   */
  public methodPatch(sendThisData: SchemaHttp): Observable<any>{
    return this.httpclient.patch( sendThisData.url , sendThisData.body ,{ headers: sendThisData.headers })
    .pipe(
      catchError((err) => {
        console.error(err);
        return throwError(err);
      })
    );
  }

}
