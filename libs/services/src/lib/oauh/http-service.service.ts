import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { HttpGeneralService } from '../general/http-service.service';
import { environment , service } from '@env/environment';
import { SchemaToken } from '@zurich-dynamic-form/schemas';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private service: HttpGeneralService) { }

  get(): Promise<string>{
    return new Promise(
      (result , reject) => {
        const headers = new HttpHeaders({
          'Authorization': 'Basic '.concat(service.token),
          'Content-Type': 'application/x-www-form-urlencoded'
        });

        this.service.methodPost({
          body : `grant_type=client_credentials`,
          url : environment.host.concat( environment.token ),
          headers : headers 
        }).subscribe(
          (response: SchemaToken) => {
            if (response && response.access_token && response.token_type){
              result( response.token_type.concat( ' '.concat(response.access_token) ) );
            } 
            reject('Error in token')
          } , reject
        );
      }
    );
  }
}
